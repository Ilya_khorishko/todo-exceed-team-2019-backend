'use strict';

const debug = require('debug')('lg:database');
const Sequelize = require('sequelize');
const path = require('path');
const klawSync = require('klaw-sync');

debug(`Database connection string: ${process.env.DATABASE_URL}`);

const sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: 'postgres',
  dialectOptions: {
    multipleStatements: true,
  },
  logging: false,
});

const models = {};
const modelsPaths = klawSync(`${__dirname}/models`, {nodir: true});
modelsPaths.forEach((file) => {
  if (!require(path.resolve(__dirname, file.path))) return;
  let model = sequelize.import(path.resolve(__dirname, file.path));
  models[model.name] = model;
});

debug(`Available models: \n\t* ${Object.keys(models).join('\n\t* ')}`);

module.exports = {sequelize, models};
