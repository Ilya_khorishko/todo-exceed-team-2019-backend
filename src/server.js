require('dotenv').load({path: process.env.DOTENV || '.env'});
const klawSync = require('klaw-sync');
const express = require('express');
const database = require('./database');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const port = (process.env.PORT || 3001);
jobs = {};
const root = __dirname;

app.use(cors({origin: '*'}));

models = require('./database').models;
app.set('models', database.models);
app.set('sequelize', database.sequelize);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
  console.info(`${new Date()}: [${req.method}] ${req.url}`);
  next();
});

useControllers();
app.use((err, req, res, next) => {
  console.error(err);
  res.status(err.status || 500).json({
    error: {
      name: err.name,
      message: err.message,
    },
  });
  next();
});

async function useControllers() {
  const paths = klawSync(`${__dirname}/controllers`, {nodir: true});
  let controllersCount = 0;
  paths.forEach((file) => {
    if (path.basename(file.path)[0] === '_' || path.basename(file.path)[0] === '.') return;
    app.use('/', require(file.path));
    controllersCount++;
  })
};

app.listen(port, '0.0.0.0', async function () {
  console.info(`${new Date()}:\n
  Server listening on port : ${port}`);
});

module.exports = app;
