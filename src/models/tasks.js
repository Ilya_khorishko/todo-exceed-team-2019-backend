module.exports = (sequalize, DataType) => {
  const tasks = sequalize.define("tasks", {
    user_id: {
      type: DataType.INTEGER,
      alowNull: false
    },
    id: {
      primaryKey: true,
      type: DataType.INTEGER,
      alowNull: false,
      unique: true,
      autoIncrement: true
    },
    text: {
      type: DataType.STRING,
      alowNull: false
    },
    status: {
      type: DataType.BOOLEAN,
      defaultValue: false,
      alowNull: false
    },
  }, {
    tableName: 'tasks',
    timestamps: false,
  });
  return tasks;
};