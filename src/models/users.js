module.exports = (sequalize, DataType) => {
  const users = sequalize.define("users", {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      unique: true,
      alowNull: false
    },
    first_name: {
      type: DataType.STRING,
      alowNull: false
    },
    last_name: {
      type: DataType.STRING,
      alowNull: false
    },
    password: {
      type: DataType.STRING,
      alowNull: false
    },
    e_mail: {
      type: DataType.STRING,
      alowNull: false,
      unique: true
    }
  }, {
    tableName: 'users',
    timestamps: false,
  });
  return users;
};