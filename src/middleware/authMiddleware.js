const jwt = require('jsonwebtoken');


module.exports = authMiddleware = () => {
  return (req, res, next) => {
    const authHeader = req.get('Authorization');
    if (!authHeader) throw errors({message: 'The problem of obtaining a token, contact support'});
    const token = authHeader.replace('Bearer ', '');
    if (!token) throw res.status(500).message('Failed to get token from header, contact technical support')
    const payload = jwt.verify(token, process.env.KEY_FOR_TOKEN);
    if (!payload) throw errors({message: "Error: Token invalid"});
    res.data = {id: payload.id, e_mail: payload.e_mail};
    next();
  }
}
