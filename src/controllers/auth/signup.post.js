const errors = require('../../errors');
const router = require('express').Router();
const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/v1/signUp',
  errors.wrap(async (req, res) => {
    const models = res.app.get('models');
    const {first_name, last_name, password, e_mail} = req.body

    const existingUser = await models.users.findOne({where: {e_mail}})
    if (existingUser) throw res.errors({message: 'Failed to create new user. a user with this e-mail already exists'});
    const bSalt = await bCrypt.genSalt(Number(process.env.HASH_SALT));
    const hashPassword = await bCrypt.hash(req.body.password, bSalt);

    const newUser = await models.users.create({
      first_name,
      last_name,
      password: hashPassword,
      e_mail
    });

    if (!newUser) throw res.errors({message: "Oops ... something went wrong, contact technical support"})
    const body = {
      id: newUser.dataValues.id,
      e_mail: newUser.dataValues.e_mail
    }

    const token = await jwt.sign(body, process.env.KEY_FOR_TOKEN, {expiresIn: 60000})
    if (!token) throw res.errors({message: 'Could not generate token'});

    res.json(token);
  }))

module.exports = router;
