const errors = require('../../errors');
const bCrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const router = require('express').Router();

router.post('/v1/signIn',
  errors.wrap(async (req, res) => {
    const models = res.app.get('models');
    const {e_mail, password} = req.body;

    const user = await models.users.findOne({where: {e_mail: e_mail}});
    if (!user) throw errors({message: 'User does not exist'});

    const isValid = await bCrypt.compare(password, user.password);
    if (!isValid) throw errors({message: 'Wrong password'})

    const body = {
      id: user.id,
      e_mail: user.e_mail
    }

    const token = jwt.sign(body, process.env.KEY_FOR_TOKEN);
    if (!token) throw errors({message: 'Token creation error, contact support'});

    res.json(token);
  }))

module.exports = router;