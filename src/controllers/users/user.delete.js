const errors = require('../../errors');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.delete('/v1/user/:id',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const models = res.app.get('models')
    const user = await models.users.findById(req.params.id);
    if (!user) throw errors({message: 'user is not registered. register and try againuser is not found'});
    if (payload.id === user.id) {
      await user.destroy(user);
      res.sendStatus(204);
    } else
      res.status(500).json({message: 'unable to delete someone else\`s account'});
  }));

module.exports = router;