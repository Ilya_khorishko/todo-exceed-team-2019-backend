const errors = require('../../errors');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.get('/v1/user/:id',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const models = res.app.get('models');
    const user = await models.users.findOne({where: {id: req.params.id}});
    if (!user) throw errors({message: 'user is not registered. register and try againuser is not found'});
    if (payload.id === user.id) {
      res.jsonp(user);
    } else
      res.status(500).json({message: 'user is not registered. register and try again'});
  }));

module.exports = router;

