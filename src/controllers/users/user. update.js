const errors = require('../../errors');
const router = require('express').Router();
const authMiddleware = require('../../middleware/authMiddleware')

router.put('/v1/user',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const models = res.app.get('models');
    const {first_name, last_name, e_mail} = req.body;
    const user = await models.users.findOne({where: {e_mail: req.query.e_mail}});

    if (!user) throw errors({message: 'user is not registered. register and try againuser is not found'});
    if (res.data.id === user.id) {
      await user.update({
        first_name,
        last_name,
        e_mail
      });
      res.sendStatus(204);
    }
    else
      res.status(500).json({message: 'it is not possible to change the settings of another account, please enter the correct id'});
  })
);

module.exports = router;