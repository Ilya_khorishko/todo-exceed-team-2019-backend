const errors = require('../../errors');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.post('/v1/task',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const models = res.app.get('models');
    const user_id = res.data.id;
    const {text} = await req.body;

    const newTask = await models.tasks.create({
      user_id,
      text
    })
    if (!newTask) throw errors({message: 'user is not found'});
    res.sendStatus(204);
  }));

module.exports = router;