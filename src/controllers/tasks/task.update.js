const router = require('express').Router();
const errors = require('../../errors');
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.put('/v1/task',
  authMiddleware(),
  errors.wrap(async (req, res) => {
    const models = res.app.get('models');
    const {text, status, vision, isupdating} = req.body;
    const task = await models.tasks.findById(req.body.id);

    authMiddleware();

    if (!task) throw errors({message: 'task is not found'});
    if (task.user_id === res.data.id) {
      await task.update({
        text,
        status,
        vision,
        isupdating
      })
      res.sendStatus(204)
    } else
      res.status(500).json({message: 'you cannot change other people\'s tasks'});


  }))

module.exports = router;