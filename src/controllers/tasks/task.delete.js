const errors = require('../../errors');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.delete('/v1/task/:id',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const task = await models.tasks.findOne({where: {id: req.params.id}})
    if (!task) throw errors({message: 'task no found'});
    if (res.data.id === task.user_id) {
      await task.destroy(task);
      res.sendStatus(204);
    } else
      res.errors({message: 'dont your task'});
  }))

module.exports = router