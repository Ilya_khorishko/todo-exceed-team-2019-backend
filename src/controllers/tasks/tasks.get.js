const errors = require('../../errors');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const authMiddleware = require('../../middleware/authMiddleware')

router.get('/v1/tasks',
  authMiddleware(),
  errors.wrap(async (req, res) => {

    const models = res.app.get('models');
    const tasks = await models.tasks.findAll({
      where: {user_id: res.data.id},
      order: [
        ['id', 'DESC']],
    })
    if (!tasks) throw errors({message: 'Could not find task, contact support'});

    res.json(tasks);
  }))

module.exports = router;